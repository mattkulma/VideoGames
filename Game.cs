﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoGames
{
    public class Game
    {
        public Game(string title, string system, string genre, string rating)
        {
            this.Title = title;
            this.System = system;
            this.Genre = genre;
            this.Rating = rating;
        }
        public string Title { get; set; }
        public string System { get; set; }
        public string Genre { get; set; }
        public string Rating { get; set; }

    }
}
