﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VideoGames
{
    class GameLibrary
    {
        public GameLibrary() { }


        List<Game> myGames = new List<Game>();

        static string folder = Environment.CurrentDirectory;
        static string file = "GameLibrary.txt";
        string location = Path.Combine(folder, file);

        public void listGames()
        {
            List<string> lines = new List<string>();
            using (StreamReader sr = File.OpenText(location))
            {
                while (!sr.EndOfStream)
                {
                    lines.Add(sr.ReadLine());
                }
            }

            foreach (var line in lines)
            {
                string[] splittedLines = line.Split('|');
                myGames.Add(new Game(splittedLines[1], splittedLines[0], splittedLines[2], splittedLines[3]));
            }
        }



        public void DisplayAll()
        {
            foreach (var kvp in myGames)
            {
                Console.WriteLine($"System: {kvp.System} Title: {kvp.Title} Genre: {kvp.Genre} Rating: {kvp.Rating}");
            }
        }


        public void AddGame()
        {
            using (StreamWriter sw = File.AppendText(location))
            {
                Console.Write("What system is this for? ");
                string system = Console.ReadLine();
                Console.Write("What is the title of the game? ");
                string title = Console.ReadLine();
                Console.Write("Wat is the games genre? ");
                string genre = Console.ReadLine();
                Console.Write("What is the games rating? ");
                string rating = Console.ReadLine();

                //its adding the new game to the list but the file is not refreshing after the game is made,
                //so it wont show the new game until the file is open and closed
                
                sw.WriteLine(system + "|" + title + "|" + genre + "|" + rating);
                
                
            }
            
        }


        //will figure this one out after i get the add game working correctly

        


    }
}
