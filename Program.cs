﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoGames
{
    class Program
    {
        static void Main(string[] args)
        {
            GameLibrary library = new GameLibrary();
            library.listGames();

            Console.WriteLine("Welcome to my game library");
            Console.WriteLine("What would you like to do?\nPlease enter a number from the list below\n"
                                    +"(1) Display a list of games.\n"
                                    +"(2) Add a game to the library.\n"
                                    +"(:q) Quit.\n");

            string input = Console.ReadLine();

            while(input != ":q")
            {
                if (input == "1")
                {
                    library.DisplayAll();
                }
                else if (input == "2")
                {
                    library.AddGame();
                }
                
                else
                {
                    Console.WriteLine("Ok, returning to main menu...");
                }



                Console.WriteLine("\nIs there anything else you would like to do?");

                Console.WriteLine("What would you like to do?\nPlease enter a number from the list below\n"
                                    + "(1) Display a list of games.\n"
                                    + "(2) Add a game to the library.\n"
                                    + "(:q) Quit.\n");
                input = Console.ReadLine();
            }

        }
    }
}
